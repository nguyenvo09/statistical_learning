options pageno=1 nodate;
ODS RTF FILE='/folders/myfolders/STAT5810/hw3.rtf';
/*Using to enable plots or not*/
ods graphics on;

title 'Discriminant Analysis of Fisher (1936) Iris Data';
title2 'Using Quadratic Discriminant Function';

proc import datafile="/folders/myfolders/STAT5810/Iris.csv" 
out=iris dbms=csv replace; 
getnames=yes; 
run;

/* proc print data=iris; */
/* run; */

proc discrim data=iris outstat=irisstat
             wcov pcov method=normal pool=test
             distance anova manova listerr crosslisterr;
   class Species;
   var SepalLength SepalWidth PetalLength PetalWidth;
run;

proc print data=irisstat;
   title2 'Output Discriminant Statistics';
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth SepalLength PetalLength PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth SepalLength PetalLength ;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  SepalLength PetalLength PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth SepalLength  PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth  PetalLength PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth SepalLength ;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth  PetalLength ;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var SepalWidth PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  SepalLength PetalLength ;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  SepalLength  PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  PetalLength  PetalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  SepalWidth;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  SepalLength;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  PetalLength;
priors proportional;
run;

proc discrim data=iris pool=NO crossvalidate;
class Species;
   var  PetalWidth;
priors proportional;
run;

FILENAME REFFILE '/folders/myfolders/STAT5810/Nest.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=NestData;
	GETNAMES=YES;
RUN;

title1 "LOGISTIC REGRESSION FOR NEST DATA"; 
title2 "Using ALL Predictor Variables"; 
proc logistic data=NestData descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType / ctable; 
roc; 
/* score data=pilotI out=pilotIpred;  */
run;

title1 "Logistic Regression With Cross-validated";
proc logistic data=NestData rocoptions(crossvalidate) plots(only)=roc;
        model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType /ctable; 
run;

title1 "Logistic Regression Without using Cross Validation";
proc logistic data=NestData plots(only)=roc;
        model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType /ctable; 
        run;



/**LDA*/
proc discrim data=NestData pool=YES crossvalidate;
class Nest;
   var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
priors proportional;
roc;
run;

/**QDA*/
proc discrim data=NestData pool=NO crossvalidate;
class Nest;
   var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
priors proportional;
run;

/**LDA feature selection (stepdisc)*/
title2 "Stepwise LDA to Select Variables by Backward Elimination"; 
proc stepdisc data=NestData method=bw; 
class Nest; 
var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType; 
run; 

/*Logistic Regression with feature selection*/
proc logistic data=NestData descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType / selection= backward sls=0.05 ; 
run;




/**Fitting model for each species separately!!!! */
DATA FlickerVsNonNest;
   SET NestData;
   IF Species in ('Flicker', 'Non-nest') ;
RUN;

DATA ChickadeeVsNonNest;
   SET NestData;
   IF Species in ('Chickadee', 'Non-nest') ;
RUN;


DATA SapsuckerVsNonNest;
   SET NestData;
   IF Species in ('Sapsucker', 'Non-nest') ;
RUN;

options pageno=1 nodate;
/*************************************************************************************************************/
/*Logistic Regression all features Flicker vs Non-Nest*/
title1 "Logistic Regression all features (Flicker vs Non-nest)";
title2 "Logistic Regression all features (Flicker vs Non-nest)";
proc logistic data=FlickerVsNonNest descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
run;

title1 "Fitting LDA all features (Flicker vs Non-Nest)";
title2 "Fitting LDA all features (Flicker vs Non-Nest)";
proc discrim data=FlickerVsNonNest pool=YES crossvalidate;
class Nest;
   var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
priors proportional;
run;

/*Logistic Regression with feature selection*/
title1 "Feature Selection with Logistic Regression (Flicker vs Non-nest)";
title2 "Feature Selection with Logistic Regression (Flicker vs Non-nest)";
proc logistic data=FlickerVsNonNest descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType / selection= backward sls=0.05; 
run;

/**LDA feature selection (stepdisc) FlickerVsNonNest*/
title1 "Feature Selection with Stepwise LDA (Flicker vs Non-nest)";
title2 "Feature Selection with Stepwise LDA (Flicker vs Non-nest)"; 
proc stepdisc data=FlickerVsNonNest method=bw; 
class Nest; 
var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType; 
run; 
/*************************************************************************************************************/


/*************************************************************************************************************/
/*Logistic Regression all features Chickadee vs Non-Nest*/
title1 "Logistic Regression all features (Chickadee vs Non-nest)";
title2 "Logistic Regression all features (Chickadee vs Non-nest)";
proc logistic data=ChickadeeVsNonNest descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
run;

title1 "Fitting LDA all features (Chickadee vs Non-Nest)";
title2 "Fitting LDA all features (Chickadee vs Non-Nest)";
proc discrim data=ChickadeeVsNonNest pool=YES crossvalidate;
class Nest;
   var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
priors proportional;
run;

/*Logistic Regression with feature selection*/
title1 "Feature Selection with Logistic Regression (Chickadee vs Non-nest)";
title2 "Feature Selection with Logistic Regression (Chickadee vs Non-nest)";
proc logistic data=ChickadeeVsNonNest descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType / selection= backward sls=0.05; 
run;

/**LDA feature selection (stepdisc) ChickadeeVsNonNest*/
title1 "Feature Selection with Stepwise LDA (Chickadee vs Non-nest)";
title2 "Feature Selection with Stepwise LDA (Chickadee vs Non-nest)"; 
proc stepdisc data=ChickadeeVsNonNest method=bw; 
class Nest; 
var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType; 
run; 
/*************************************************************************************************************/



/*************************************************************************************************************/
/*Logistic Regression all features Sapsucker vs Non-Nest*/
title1 "Logistic Regression all features (Sapsucker vs Non-nest)";
title2 "Logistic Regression all features (Sapsucker vs Non-nest)";
proc logistic data=SapsuckerVsNonNest descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
run;

title1 "Fitting LDA all features (Sapsucker vs Non-Nest)";
title2 "Fitting LDA all features (Sapsucker vs Non-Nest)";
proc discrim data=SapsuckerVsNonNest pool=YES crossvalidate;
class Nest;
   var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType;
priors proportional;
run;

/*Logistic Regression with feature selection*/
title1 "Feature Selection with Logistic Regression (Sapsucker vs Non-nest)";
title2 "Feature Selection with Logistic Regression (Sapsucker vs Non-nest)";
proc logistic data=SapsuckerVsNonNest descending; 
model Nest = NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType / selection= backward sls=0.05; 
run;

/**LDA feature selection (stepdisc) SapsuckerVsNonNest*/
title1 "Feature Selection with Stepwise LDA (Sapsucker vs Non-nest)";
title2 "Feature Selection with Stepwise LDA (Sapsucker vs Non-nest)"; 
proc stepdisc data=SapsuckerVsNonNest method=bw; 
class Nest; 
var  NumTreelt1in NumTree1to3in NumTree3to6in NumTree6to9in NumTree9to15in NumTreegt15in NumSnags NumDownSnags PctShrubCover NumConifer StandType; 
run; 
/*************************************************************************************************************/


ods graphics off;
ODS RTF CLOSE;