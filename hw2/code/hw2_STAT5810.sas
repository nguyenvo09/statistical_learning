options pageno=1 nodate;
ODS RTF FILE='/folders/myfolders/STAT5810/hw2.rtf';
/*Using to enable plots or not*/
ods graphics on;

proc import datafile="/folders/myfolders/STAT5810/Full-Air-Pollution-Data.csv" 
out=AirPol dbms=csv replace; 
getnames=yes; 
run;

/*1a: checking variances of residuals are constant (i.e. homogeneous) or non-constant (i.e. heteoscedasitc)*/
title2 "Ordinary Linear Regression  Heteroscedasticity, ";
proc reg data=AirPol ;
     model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity /white;
	output out=AirPolres (keep= Mortality r fv) residual=r predicted=fv;
run;
title2 "Testing Normality of Residuals";
/*1a: Normality of residuals*/
proc univariate data=AirPolres normal;
 var r;
 qqplot r / normal(mu=est sigma=est);
run;

/* 1b Colinearity between variables: Result: HC and NOX are worriesome 
@ref: http://www.ats.ucla.edu/stat/sas/webbooks/reg/chapter2/sasreg2.htm */
title2 "Colinerity Analysis";
ods graphics off;
proc reg data=AirPol ;
     model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity /vif tol;
run;

/*1c: backward elemination using PROC GLMSELECT
@ref: https://support.sas.com/documentation/cdl/en/statug/63347/HTML/default/viewer.htm#statug_glmselect_a0000000240.htm
http://www2.sas.com/proceedings/sugi31/207-31.pdf
selection=backward
removes effects that at each step produce the largest value of the Schwarz Bayesian information criterion (SBC) 
statistic and stops at the step where removing any effect increases the SBC statistic.
This data will be used for 1d question. Hardcode!!!!!
*/
ods graphics on;
proc glmselect data=AirPol;
   model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity/selection=backward; 
run;
/* proc print data=AirPolReduced; run; */

/*Suprisingly, backward elimination in "proc reg" has different results from "proc glmselect"*/
/*
ods graphics off;
proc reg data=AirPol;
	   model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity/selection=backward; 
run; */

/*1d: ridge regression for data after backward elimination. It is a way for feature selection. But, Ridge regression will keep all 
predicters/features/variables in the end except when \lambda =+Infinity. It is a disadvantage of this method!!!!
http://www2.sas.com/proceedings/sugi31/207-31.pdf
*/
title2 "1d. Ridge regression";
ods graphics on;
proc reg data=AirPol outvif outest=coefficients ridge=0 to 0.15 by 0.01; 
model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity;
run; 

proc print data = coefficients;
var _RIDGE_ Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity;
where _TYPE_ = 'RIDGEVIF';
run;
proc print data = coefficients;
var _RIDGE_ _RMSE_ Intercept Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity;
where _TYPE_ = 'RIDGE';
run;

/* proc print data=coefficients;  */
/* run;  */


/*1e: Lasso*/
title1 "LASSO Regression"; 
proc glmselect data=AirPol plots=coefficients; 
model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity / selection=LASSO(choose=CV) stop=20 CVmethod=split(5); 
run;

/*1e: elastic net*/
title1 "Elasticnet Regression"; 
proc glmselect data=AirPol plots=coefficients; 
model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2 Precip AveJanTemp AveJulTemp Humidity / selection=Elasticnet(choose=CV) stop=20 CVmethod=split(5); 
run;

ods graphics off;
ODS RTF CLOSE;
