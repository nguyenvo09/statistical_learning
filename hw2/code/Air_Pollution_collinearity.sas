options nodate pageno=1;
libname s5100 'C:\Users\Richard\Documents\Classes\Stat 5100\Fall 2010\sasdata'; 
run;

ods rtf file='C:\Users\Richard\Documents\Classes\Stat 5100\2017 Spring STAT 5100 001\Air Pollution collinearity.rtf';
ods graphics on;

data AirPol;
     infile 'C:\Users\Richard\Documents\Classes\Stat 5100\2017 Spring STAT 5100 001\Air Pollution.txt';
	 format SMSA $char12.;
	 input Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor Mortality popden
           HC NOX SO2 SMSA $ State $ Precip AveJanTemp AveJulTemp Humidity ID;
	 label Pctover65 = "Percent of Population over 65 years of age";
	 label PopperHse = "Population per household";
     label Education = "Median education of persons aged 25 and older";
     label PctGoodHse = "Percentage of sound housing units";
     label NonWhite = "Percentage of non-white persons in population";
     label WhiteCol = "Percentage of white collar employment";
     label PctPoor = "Percentage of families with income < $3000";
     label Mortality = "Age-adjusted mortality (per 100,000 persons in population)";
     label popden = "Population Density (number of People per square mile)";
     label HC = "Relative HC pollution potential";
     label NOX = "Relative nitrous oxide (NO) pollution potential";
     label SO2 = "Relative sulphur dioxide (SO2) pollution potential";
     label SMSA = "SMSA (City)";
     label Precip = "Average yearly precipitation (in inches)";
     label AveJanTemp = "Average January Temperature (in degrees Fahrenheit)";
     label AveJulTemp = "Average July Temperature (in degrees Fahrenheit)";
     label Humidity = "Average Relative Humidity (in percent)";
     label ID = "Observation Number";
	 logMort = log(Mortality);
run;
title2 "Checking for Multicollinearity";
proc reg data=AirPol plots=none;
     model Mortality = Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2/ collin VIF;
run;

proc corr data=AirPol plots=matrix(nvar=all histogram);
     var Pctover65 PopperHse Education PctGoodHse NonWhite WhiteCol PctPoor popden HC NOX SO2;
run;

ods graphics off;
ods rtf close;

quit;
