---
title: "Homework 1 - STAT5810"
author: "Nguyen Vo - A02213331"
output:
  pdf_document:
    fig_caption: yes
    keep_tex: yes
  word_document: default
header-includes: \usepackage{amsmath}
---

```{r include = FALSE}
library(viridis)
library(ggplot2)
library(reshape2)
library(gridExtra)
library(corrplot)
#library(cowplot)
```

## Q1: Graphically summarize the distributions of the three variables using boxplots, histograms, and normal quantile plots. Do the summaries for the combined data and for each gender of turtle. 
## Boxplot, histogram, normal quantile plot for combined data
```{r}
turtles <- read.csv('turtles.csv', header = TRUE)

#boxplot
boxplot(turtles[0:3] )

#histogram
d <- melt(turtles[,c("Length", "Width", "Height")])
ggplot(d,aes(x = value)) + 
    facet_wrap(~variable,scales = "free_x") + 
    geom_histogram() + ggtitle('Combined data') 

#normal quantile plots
p1 <- ggplot(turtles, aes(sample=turtles$Length )) +stat_qq() + ggtitle('Length')
p2 <- ggplot(turtles, aes(sample=turtles$Width )) +stat_qq() + ggtitle('Width')
p3 <- ggplot(turtles, aes(sample=turtles$Height )) +stat_qq() + ggtitle('Height')
grid.arrange(p1, p2, p3, ncol=3, nrow =1)

#summary of combined data
summary(turtles[0:3])
```

## Boxplot, histogram, normal quantile plot for male turtles

```{r}
males = turtles[turtles[, "Gender"] == 'male',]
boxplot(males[0:3] )

d1 <- melt(males[,c("Length", "Width", "Height")])
ggplot(d1,aes(x = value)) + 
    facet_wrap(~variable,scales = "free_x") + 
    geom_histogram() + ggtitle('Male turtles')

p1 <- ggplot(males, aes(sample=males$Length )) +stat_qq() + ggtitle('Length')
p2 <- ggplot(males, aes(sample=males$Width )) +stat_qq() + ggtitle('Width')
p3 <- ggplot(males, aes(sample=males$Height )) +stat_qq() + ggtitle('Height')
grid.arrange(p1, p2, p3, ncol=3, nrow =1)

#summary of male turtles
summary(males[0:3])
```

## Boxplot, histogram, normal quantile plot for female turtles

```{r}
females = turtles[turtles[, "Gender"] == 'female',]
boxplot(females[0:3] )

d2 <- melt(females[,c("Length", "Width", "Height")])
ggplot(d2,aes(x = value)) + 
    facet_wrap(~variable,scales = "free_x") + 
    geom_histogram()+ ggtitle('Female turtles')

p1 <- ggplot(females, aes(sample=females$Length )) +stat_qq() + ggtitle('Length')
p2 <- ggplot(females, aes(sample=females$Width )) +stat_qq() + ggtitle('Width')
p3 <- ggplot(females, aes(sample=females$Height )) +stat_qq() + ggtitle('Height')
grid.arrange(p1, p2, p3, ncol=3, nrow =1)

#summary of male turtles
summary(females[0:3])
```
In summary, we can see that female turtles are bigger than male turtles in terms of length, width and height. 

## Q2: Compute the covariance matrices and the correlation matrices for the male and female turtles, and visually compare them.
Male turtles:
```{r}
#covariance matrix of male turtles
cov(males[0:3], y=males[0:3], use="all.obs")
#correlation matrix of male turtles, diagonal should be 1, other elements in [-1;1]
M1 <- cor(males[0:3], y=males[0:3], use="all.obs")
M1
#visualize this matrix
corrplot(M1, method="color") + ggtitle("Correlation plot of male turtles")
```
Female turtles:
```{r}
#covariance matrix of male turtles
cov(females[0:3], y=females[0:3], use="all.obs")
#correlation matrix of male turtles, diagonal should be 1, other elements in [-1;1]
M2<- cor(females[0:3], y=females[0:3], use="all.obs")
M2
#visualize this matrix
corrplot(M2, method="color")
```

## Q3: For one of the genders, compute the matrix ... and compare it with the covariance matrix you previously obtained.
For male turtles:
```{r}
X <- as.matrix(males[0:3])
no_obs <- dim(X)[1]
x_bar <- as.matrix(colMeans(X))
Male_cov_hand <- 1.0 / (no_obs-1) * (t(X)%*%X - no_obs * x_bar %*% t(x_bar))
M1 <- cov(males[0:3], y=males[0:3], use="all.obs")
#check whether covariance matrix of male turtles is exactly same as matrix Male_cov_hand
stopifnot(sum(abs(Male_cov_hand - M1)) < 1e-10) 
Male_cov_hand
```
For female turtles:
```{r}
X <- as.matrix(females[0:3])
no_obs <- dim(X)[1]
x_bar <- as.matrix(colMeans(X))
Female_cov_hand <- 1.0 / (no_obs-1) * (t(X)%*%X - no_obs * x_bar %*% t(x_bar))
M1 <- cov(females[0:3], y=females[0:3], use="all.obs")
#covariance matrix of female turtles is same as matrix Female_cov_hand
stopifnot(sum(abs(Female_cov_hand - M1)) < 1e-10) 
Female_cov_hand
```

## Q4: For one of the covariance matrices, compute the eigenvalues andeigenvectors of the inverse of the covariance matrix. What is the relationship between three eigenvalues and eigenvectors of a covariance matrix and its inverse?

```{r}
M1 <- cov(males[0:3], y=males[0:3], use="all.obs")
M1 <- as.matrix(M1)
InvCov <- solve(M1)
#finding eigenvectors and eigenvalues of inverse of covariance matrix.
r <- eigen(InvCov, symmetric = TRUE)
Q<-r$vectors
L<-diag(r$values)
#InvCov = QLQ^T
stopifnot(sum(abs(Q %*% L %*% t(Q) - InvCov)) < 1e-10)
r
```
\begin{table}[h]
\centering
\caption{Eigenvalues and eigenvectors}
\label{tbl:q4}
\begin{tabular}{|l|l|}
\hline
Eigenvalues & Eigenvectors                            \\ \hline
0.905934472 & {[}0.23653541 0.04687583 -0.97049145{]} \\ \hline
0.271108226 & {[}0.48810477 -0.86938426 0.07697229{]} \\ \hline
0.005120993 & {[}0.8401219 0.4919082 0.2285205{]}     \\ \hline
\end{tabular}
\end{table}
Suppose Q is matrix of eigenvectors of covariance matrix M. Because M is symmetric, Q is orthogonal (i.e. $Q^T = Q^{-1}$). $\Lambda$ is diagonal matrix of eigenvalues of M. Because $M=Q \cdot \Lambda \cdot Q^T$, inverse of covariance matrix M is $M^{-1} = (Q \cdot \Lambda \cdot Q^T)^{-1} = Q\cdot \Lambda^{-1}\cdot Q^T$. We can observe that eigenvectors of $M$ are also eigenvectors of $M^{-1}$

## Q5: S is covariance matrix. Letting denote the eigenvector corresponding to the largest eigenvector. Verify that : It's too obvious.
```{r}
S=cov(turtles[0:3], y=turtles[0:3])
info=eigen(S, symmetric = TRUE)
lambda1=info$values[1]
eigen_vector1=info$vectors[,1]
stopifnot(sum(abs(S %*% eigen_vector1 - lambda1 * eigen_vector1)) < 1e-10)
S %*% eigen_vector1
lambda1 * eigen_vector1
t(eigen_vector1) %*% S %*% eigen_vector1 
lambda1
stopifnot(abs(t(eigen_vector1) %*% S %*% eigen_vector1  - lambda1) < 1e-10)
```



## Q6: The length, width, and height are all measured in the same units. Is there any reason we might prefer to use the correlation matrices over the covariance matrices if we were to carry out principal components analysis on these data? 

When scale of variables (i.e. features) are on different scales, we might prefer to use correlation matrices because correlation matrices standardize our data (e.g., entries in correlation matrix is in [-1;1]). 

## Q7: Compute the eigenvalues and eigenvectors for the covariance and the correlation matrix for one of the genders of turtle, and compare the eigenvectors. In each case, how many principal components would you recommend retaining? 

```{r}
library("factoextra")
#I use male turtles dataset.
cov_male <- cov(males[0:3], y=males[0:3], use="all.obs")
cov_male <- as.matrix(cov_male)
cor_male = cor(males[0:3], y=males[0:3], use="all.obs")
cor_male <- as.matrix(cor_male)
# print(cov_male)
# print(cor_male)
e_cov = eigen(cov_male)
print(e_cov$values)
print(e_cov$vectors)

e_cor = eigen(cor_male)
print(e_cor$values)
print(e_cor$vectors)
print(e_cor$vectors[,1])
print(e_cor$vectors[,2])
#pca <- princomp(cor_male)
#summary(pca)
#For correlation matrix of male turtles
res.pca <- prcomp(cor_male)
get_eig(res.pca)
fviz_eig(res.pca) + ggtitle("Scree plot of three pricipal component of correlation matrix of male turles")

#For covariance matrix of male turtles
res.pca <- prcomp(cov_male)
get_eig(res.pca)
fviz_eig(res.pca) + ggtitle("Scree plot of three pricipal component of covariance matrix of male turles")


```

For covariance matrix of male turtles, the eigen values are 195.274633, 3.688564 and 1.103833. The largest eigenvalue is 195.27 which is 53 times larger than other the second largest eigenvalue. Therefore, I keep only one principal component which has eigenvalue=195.27 and eigenvector=(0.8401219, 0.4919082, 0.2285205)

For correlation matrix, the largest eigenvalue is 2.87183776 which is 33 times larger than other the second largest eigenvalue. Therefore, I keep only one principal component which has eigenvalue=2.87183776 and eigenvector=(0.5821538, 0.5753752, 0.5744914)

