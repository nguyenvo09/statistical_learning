
def process_data():
    fin=open('Nest.csv', 'rb')
    fout=open("Nest_removed.csv", 'wb')
    firstLine = True
    for line in fin:
        if firstLine:
            fout.write(line)
            firstLine = False
            continue

        Nest, Species, NumTreelt1in, NumTree1to3in, NumTree3to6in, NumTree6to9in, NumTree9to15in, NumTreegt15in, NumSnags, NumDownSnags, PctShrubCover, NumConifer, StandType = line.split(',')
        if Species == 'Non-nest':
            continue
        fout.write('%s' % line)

if __name__ == '__main__':
    process_data()