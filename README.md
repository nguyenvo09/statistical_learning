# README #

This repository contains reports and R source code for Statistical Learning class STAT5810 at Utah State University. 
Various machine learning algorithms learned (Random Forest, classification tree, LDA, QDA etc). Final project is focused on data analysis of malicious retweeter groups and classication. 

### Homework 1 ###

* SAS and R tutorial 
* Pricipal Component Analysis in R
* Data anlysis of Turtle dataset in R

### Homework 2 ###

* Feature selection SAS with GLMSELECT procedure in dataset Air Pollution 
* Ridge Regression
* Lasso - L1 norm
* Elastic net - L1 + L2 norms

### Homework 3 ###

* Linear discriminant analysis
* Quadratic discriminant analysis.
* Cross‐validation 

### Homework 4 ###

* bootstrap 
* Classification Tree, LDA, QDA
* Random Forest 

### Project ###
 [Link Report](https://drive.google.com/file/d/0BzJ4lzc40toAdDl0cFFGX082aXM/view?usp=sharing/)